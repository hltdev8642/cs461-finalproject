void regenerateMesh()
{
    offset = transform.position;
    Mesh subMesh = this.GetComponent<MeshFilter>().mesh;
    MeshCollider mc = gameObject.GetComponent<MeshCollider>();
    List<int> indices = new List<int>();
    List<Vector2> uvs = new List<Vector2>();
    List<Vector3> verts = new List<Vector3>();
    List<Color> colors = new List<Color>();
    subMesh.Clear();
    int curVert = 0;
    int density = 0;
    int[] faceIDs;
    Vector3 curVec = new Vector3(0, 0, 0);
    for (int x = 0; x < chunkSize.x; x++)
    {
        for (int y = 0; y < chunkSize.y; y++)
        {
            for (int z = 0; z < chunkSize.z; z++)
            {
                curVec.x = x;
                curVec.y = y;
                curVec.z = z;
                density = getTerrainValue(curVec);
                m_tmpdata[(int)curVec.x, (int)curVec.y, (int)curVec.z] = 0;
                if (density == 0 )
                    continue;
                faceIDs = GetBlockID(new Vector3(x, y + 1, z), new Vector3(x, y, z), density);
                curVec.z--;
                // Back
                if (getTerrainValue(curVec) == 0)
                {
                    verts.Add(new Vector3(x, y, z));
                    verts.Add(new Vector3(x + 1, y + 1, z));
                    verts.Add(new Vector3(x + 1, y, z));
                    verts.Add(new Vector3(x, y + 1, z));
                    addUV(faceIDs[0], ref uvs);
                    indices.Add(curVert);
                    indices.Add(curVert + 1);
                    indices.Add(curVert + 2);
                    indices.Add(curVert);
                    indices.Add(curVert + 3);
                    indices.Add(curVert + 1);
                    curVert += 4;
                }
                curVec.z += 2;
                // Front
                if (getTerrainValue(curVec) == 0)
                {
                    verts.Add(new Vector3(x, y, z + 1));
                    verts.Add(new Vector3(x + 1, y + 1, z + 1));
                    verts.Add(new Vector3(x + 1, y, z + 1));
                    verts.Add(new Vector3(x, y + 1, z + 1));
                    addUV(faceIDs[1], ref uvs);
                    indices.Add(curVert);
                    indices.Add(curVert + 2);
                    indices.Add(curVert + 1);
                    indices.Add(curVert);
                    indices.Add(curVert + 1);
                    indices.Add(curVert + 3);
                    curVert += 4;
                }
                curVec.z = z;
                curVec.y--;
                // Bottom
                if (getTerrainValue(curVec) == 0)
                {
                    verts.Add(new Vector3(x, y, z));
                    verts.Add(new Vector3(x + 1, y, z + 1));
                    verts.Add(new Vector3(x + 1, y, z));
                    verts.Add(new Vector3(x, y, z + 1));
                    addUV(faceIDs[2], ref uvs);
                    indices.Add(curVert);
                    indices.Add(curVert + 2);
                    indices.Add(curVert + 1);
                    indices.Add(curVert);
                    indices.Add(curVert + 1);
                    indices.Add(curVert + 3);
                    curVert += 4;
                }
                curVec.y += 2;
                //shade=0.5f;
                // Top
                if (getTerrainValue(curVec) == 0)
                {
                    verts.Add(new Vector3(x, y + 1, z));
                    verts.Add(new Vector3(x + 1, y + 1, z + 1));
                    verts.Add(new Vector3(x + 1, y + 1, z));
                    verts.Add(new Vector3(x, y + 1, z + 1));
                    addUV(faceIDs[3], ref uvs);
                    indices.Add(curVert);
                    indices.Add(curVert + 1);
                    indices.Add(curVert + 2);
                    indices.Add(curVert);
                    indices.Add(curVert + 3);
                    indices.Add(curVert + 1);
                    curVert += 4;
                }
                curVec.y = y;
                //shade = 0.45f;
                curVec.x--;
                // Left
                if (getTerrainValue(curVec) == 0)
                {
                    verts.Add(new Vector3(x, y, z + 1));
                    verts.Add(new Vector3(x, y + 1, z));
                    verts.Add(new Vector3(x, y, z));
                    verts.Add(new Vector3(x, y + 1, z + 1));
                    addUV(faceIDs[4], ref uvs);
                    indices.Add(curVert);
                    indices.Add(curVert + 3);
                    indices.Add(curVert + 1);
                    indices.Add(curVert);
                    indices.Add(curVert + 1);
                    indices.Add(curVert + 2);
                    curVert += 4;
                }
                curVec.x += 2;
                // Right
                if (getTerrainValue(curVec) == 0)
                {
                    verts.Add(new Vector3(x + 1, y, z + 1));
                    verts.Add(new Vector3(x + 1, y + 1, z));
                    verts.Add(new Vector3(x + 1, y, z));
                    verts.Add(new Vector3(x + 1, y + 1, z + 1));
                    addUV(faceIDs[5], ref uvs);
                    indices.Add(curVert);
                    indices.Add(curVert + 1);
                    indices.Add(curVert + 3);
                    indices.Add(curVert);
                    indices.Add(curVert + 2);
                    indices.Add(curVert + 1);
                    curVert += 4;
                }
            }
        }
    }
    subMesh.vertices = verts.ToArray();
    subMesh.triangles = indices.ToArray();
    subMesh.uv = uvs.ToArray();
    //subMesh.colors = colors.ToArray();
    //subMesh.Optimize();
    subMesh.RecalculateNormals();
    //if (mc.sharedMesh == null) mc.sharedMesh = subMesh;
    mc.sharedMesh = new Mesh();
    mc.sharedMesh = subMesh;
    //mc.sharedMesh.Optimize();
}
