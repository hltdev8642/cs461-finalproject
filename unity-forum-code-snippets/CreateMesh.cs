public void CreateMesh()
{
    this.InitLists();
    for (int x = (int)this.size.x - 1; x >= 0; x--)
    {
        for (int y = (int) this.size.y - 1; y >= 0 ; y--)
        {
            for (int z = (int)this.size.z - 1; z >= 0; z--)
            {
                //process non-empty voxels
                if (this.data[x, y, z] != 0)
                {
                    float h = 0.5f;
                    /**
                     *         VERTEX NUMBER/NAMES
                     *
                     *                     1(LUF)         2(RUF)
                     *                       *--------------*
                     *                      /|             /|
                     *   y           8(LUB)/ |      7(RUB)/ |
                     *   |                *--------------*  |
                     *   |                |  |           |  |
                     *   |_____ x         |  |           |  |
                     *   /                |  |4(LLF)     |  |
                     *  /                 |  *-----------|--*3(RLF)
                     *   -z               | /            | /
                     *                    |/             |/
                     *              5(LLB)*--------------*6(RLB)
                     *
                     *       Unity likes Clockwise vertex winding
                     *
                     * */
                    Vector3 LUF = new Vector3(x - this.size.x / 2 - h, y + h, z - this.size.z / 2 + h);
                    Vector3 LUB = new Vector3(x - this.size.x / 2 - h, y + h, z - this.size.z / 2 - h);
                    Vector3 LLF = new Vector3(x - this.size.x / 2 - h, y - h, z - this.size.z / 2 + h);
                    Vector3 LLB = new Vector3(x - this.size.x / 2 - h, y - h, z - this.size.z / 2 - h);
                    Vector3 RUF = new Vector3(x - this.size.x / 2 + h, y + h, z - this.size.z / 2 + h);
                    Vector3 RUB = new Vector3(x - this.size.x / 2 + h, y + h, z - this.size.z / 2 - h);
                    Vector3 RLF = new Vector3(x - this.size.x / 2 + h, y - h, z - this.size.z / 2 + h);
                    Vector3 RLB = new Vector3(x - this.size.x / 2 + h, y - h, z - this.size.z / 2 - h);
                    Vector2 zeroOne = new Vector2(0, 1);
                    Vector2 zeroZero = new Vector2(0, 0);
                    Vector2 oneOne = new Vector2(1, 1);
                    Vector2 oneZero = new Vector2(1, 0);
                    //left side
                    if (x == 0)
                    {
                        if (this.left != null  (this.left.GetDensityAt((int) this.size.x - 1, y, z) == 0))
                        {
                            this.AddVertexAndTriangleIndex(LUB, oneOne, Vector3.left);
                            this.AddVertexAndTriangleIndex(LLF, zeroZero, Vector3.left);
                            this.AddVertexAndTriangleIndex(LUF, zeroOne, Vector3.left);
                            this.AddVertexAndTriangleIndex(LLB, oneZero, Vector3.left);
                            this.AddVertexAndTriangleIndex(LLF, zeroZero, Vector3.left);
                            this.AddVertexAndTriangleIndex(LUB, oneOne, Vector3.left);
                        }
                    }
                    else if (this.data[x - 1, y, z] == 0)
                    {
                        this.AddVertexAndTriangleIndex(LUB, oneOne, Vector3.left);
                        this.AddVertexAndTriangleIndex(LLF, zeroZero, Vector3.left);
                        this.AddVertexAndTriangleIndex(LUF, zeroOne, Vector3.left);
                        this.AddVertexAndTriangleIndex(LLB, oneZero, Vector3.left);
                        this.AddVertexAndTriangleIndex(LLF, zeroZero, Vector3.left);
                        this.AddVertexAndTriangleIndex(LUB, oneOne, Vector3.left);
                    }
                    //right side
                    if (x == this.size.x - 1)
                    {
                        if (this.right != null  (this.right.GetDensityAt(0, y, z) == 0))
                        {
                            this.AddVertexAndTriangleIndex(RUB, zeroOne, Vector3.right);
                            this.AddVertexAndTriangleIndex(RUF, oneOne, Vector3.right);
                            this.AddVertexAndTriangleIndex(RLF, oneZero, Vector3.right);
                            this.AddVertexAndTriangleIndex(RLF, oneZero, Vector3.right);
                            this.AddVertexAndTriangleIndex(RLB, zeroZero, Vector3.right);
                            this.AddVertexAndTriangleIndex(RUB, zeroOne, Vector3.right);
                        }
                    }
                    else if (this.data[x + 1, y, z] == 0)
                    {
                        this.AddVertexAndTriangleIndex(RUB, zeroOne, Vector3.right);
                        this.AddVertexAndTriangleIndex(RUF, oneOne, Vector3.right);
                        this.AddVertexAndTriangleIndex(RLF, oneZero, Vector3.right);
                        this.AddVertexAndTriangleIndex(RLF, oneZero, Vector3.right);
                        this.AddVertexAndTriangleIndex(RLB, zeroZero, Vector3.right);
                        this.AddVertexAndTriangleIndex(RUB, zeroOne, Vector3.right);
                    }
                    //bottom
                    if (y == 0)
                    {
                        if (this.bottom != null  this.bottom.GetDensityAt(x, (int) this.size.y - 1, z) == 0)
                        {
                            this.AddVertexAndTriangleIndex(LLF, zeroZero, Vector3.down);
                            this.AddVertexAndTriangleIndex(LLB, zeroOne, Vector3.down);
                            this.AddVertexAndTriangleIndex(RLB, oneOne, Vector3.down);
                            this.AddVertexAndTriangleIndex(RLB, oneOne, Vector3.down);
                            this.AddVertexAndTriangleIndex(RLF, oneZero, Vector3.down);
                            this.AddVertexAndTriangleIndex(LLF, zeroZero, Vector3.down);
                        }
                    }
                    else if (this.data[x, y - 1, z] == 0)
                    {
                        this.AddVertexAndTriangleIndex(LLF, zeroZero, Vector3.down);
                        this.AddVertexAndTriangleIndex(LLB, zeroOne, Vector3.down);
                        this.AddVertexAndTriangleIndex(RLB, oneOne, Vector3.down);
                        this.AddVertexAndTriangleIndex(RLB, oneOne, Vector3.down);
                        this.AddVertexAndTriangleIndex(RLF, oneZero, Vector3.down);
                        this.AddVertexAndTriangleIndex(LLF, zeroZero, Vector3.down);
                    }
                    //top
                    if (y == this.size.y - 1)
                    {
                        if ( this.top == null || this.top.GetDensityAt(x, 0, z) == 0)
                        {
                            this.AddVertexAndTriangleIndex(LUF, zeroOne, Vector3.up);
                            this.AddVertexAndTriangleIndex(RUF, oneOne, Vector3.up);
                            this.AddVertexAndTriangleIndex(RUB, oneZero, Vector3.up);
                            this.AddVertexAndTriangleIndex(RUB, oneZero, Vector3.up);
                            this.AddVertexAndTriangleIndex(LUB, zeroZero, Vector3.up);
                            this.AddVertexAndTriangleIndex(LUF, zeroOne, Vector3.up);
                        }
                    }
                    else if (this.data[x, y + 1, z] == 0)
                    {
                        this.AddVertexAndTriangleIndex(LUF, zeroOne, Vector3.up);
                        this.AddVertexAndTriangleIndex(RUF, oneOne, Vector3.up);
                        this.AddVertexAndTriangleIndex(RUB, oneZero, Vector3.up);
                        this.AddVertexAndTriangleIndex(RUB, oneZero, Vector3.up);
                        this.AddVertexAndTriangleIndex(LUB, zeroZero, Vector3.up);
                        this.AddVertexAndTriangleIndex(LUF, zeroOne, Vector3.up);
                    }
                    //Back
                    if (z == 0)
                    {
                        if (this.back != null  this.back.GetDensityAt(x, y, (int) this.size.z - 1) == 0)
                        {
                            this.AddVertexAndTriangleIndex(LLB, zeroZero, Vector3.back);
                            this.AddVertexAndTriangleIndex(LUB, zeroOne, Vector3.back);
                            this.AddVertexAndTriangleIndex(RUB, oneOne, Vector3.back);
                            this.AddVertexAndTriangleIndex(RUB, oneOne, Vector3.back);
                            this.AddVertexAndTriangleIndex(RLB, oneZero, Vector3.back);
                            this.AddVertexAndTriangleIndex(LLB, zeroZero, Vector3.back);
                        }
                    }
                    else if (this.data[x, y, z - 1] == 0)
                    {
                        this.AddVertexAndTriangleIndex(LLB, zeroZero, Vector3.back);
                        this.AddVertexAndTriangleIndex(LUB, zeroOne, Vector3.back);
                        this.AddVertexAndTriangleIndex(RUB, oneOne, Vector3.back);
                        this.AddVertexAndTriangleIndex(RUB, oneOne, Vector3.back);
                        this.AddVertexAndTriangleIndex(RLB, oneZero, Vector3.back);
                        this.AddVertexAndTriangleIndex(LLB, zeroZero, Vector3.back);
                    }
                    //FRONT
                    if (z == this.size.z - 1)
                    {
                        if (this.front != null  this.front.GetDensityAt(x, y, 0) == 0)
                        {
                            this.AddVertexAndTriangleIndex(LUF, oneOne, Vector3.forward);
                            this.AddVertexAndTriangleIndex(LLF, oneZero, Vector3.forward);
                            this.AddVertexAndTriangleIndex(RUF, zeroOne, Vector3.forward);
                            this.AddVertexAndTriangleIndex(LLF, oneZero, Vector3.forward);
                            this.AddVertexAndTriangleIndex(RLF, zeroZero, Vector3.forward);
                            this.AddVertexAndTriangleIndex(RUF, zeroOne, Vector3.forward);
                        }
                    }
                    else if (this.data[x, y, z + 1] == 0)
                    {
                        this.AddVertexAndTriangleIndex(LUF, oneOne, Vector3.forward);
                        this.AddVertexAndTriangleIndex(LLF, oneZero, Vector3.forward);
                        this.AddVertexAndTriangleIndex(RUF, zeroOne, Vector3.forward);
                        this.AddVertexAndTriangleIndex(LLF, oneZero, Vector3.forward);
                        this.AddVertexAndTriangleIndex(RLF, zeroZero, Vector3.forward);
                        this.AddVertexAndTriangleIndex(RUF, zeroOne, Vector3.forward);
                    }
                }
            }
        }
    }
    MeshCollider mCollider = this.GetComponent<MeshCollider>();
    MeshFilter mFilter = this.GetComponent<MeshFilter>();
    mFilter.mesh.Clear();
    if (mCollider.sharedMesh != null)
    {
        mCollider.sharedMesh.Clear();
        mCollider.sharedMesh = null;
    }
    //Destroy(mFilter.mesh);
    //Destroy(mCollider.sharedMesh);
    //mFilter.mesh = new Mesh();
    mFilter.mesh.vertices = this.vertices.ToArray();
    mFilter.mesh.uv = this.UVs.ToArray();
    mFilter.mesh.normals = this.normals.ToArray();
    mFilter.mesh.triangles = this.triangles.ToArray();
    mFilter.mesh.RecalculateNormals();
    mFilter.mesh.Optimize();
    mCollider.sharedMesh = mFilter.mesh;
}
