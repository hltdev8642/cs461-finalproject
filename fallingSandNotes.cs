/*
[Set of "rules" for downward flowing sandbox particles]

	For each entity that is affected by gravity
		|- If there’s an empty space below it, move it down.
		|- If there’s an empty space down and to the left, move it down and to the left.
		|- If there’s an empty space down and to the right, move it down and to the right.

		[ ] [ ] [ ]

		[ ] [X] [ ]
			  ↧
		[ ] [ ] [ ]


		[ ] [ ] [ ]

		[ ] [X] [ ]
		   ↙  ↘
		[ ] [*] [ ]
*/

/*
Example 1:
	0 --> AIR
	1 --> SAND
	2 --> WATER

  -
  - [0][0][0][0][0][0][0]
  - [0][0][0][0][0][0][0]
  - [0][0][0][0][0][0][0]
  - [2][2][2][2][2][0][0]
  y [2][1][1][2][2][2][1]
  - [2][1][1][2][2][2][1]
  - [1][1][1][1][1][2][1]
  - [1][1][1][1][1][1][1]
  - [1][1][1][1][1][1][1]
  - - - - - - - - - - - - -
              x

This solution "kind of works".
*/

/*
Another method to use for calculations:
https://en.wikipedia.org/wiki/Quadtree
*/

