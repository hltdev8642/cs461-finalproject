[](/)

*   [Products](https://unity.com/products)
*   [Solutions](https://unity.com/solutions)
*   [Made with Unity](https://unity.com/madewith)
*   [Learning](https://unity.com/learn)
*   [Support & Services](https://unity.com/support-services)
*   [Community](https://unity3d.com/community)
*   [Asset Store](https://unity3d.com/asset-store)
*   [Get Unity](https://unity3d.com/get-unity)

<a class="tool tool-menu toggle-panel" data-panel="mobile-navigation"></a><a class="tool tool-search toggle-panel" data-panel="search-panel"></a><a class="tool user-icon toggle-panel" data-panel="user-panel">![](https://unity3d.com/profiles/unity3d/themes/unity/images/ui/icons/other/user-default-light64x64.png)</a> 

#### Search Unity

<input type="text" class="sj-search-bar-input-common" placeholder="Search...">![](https://unity3d.com/profiles/unity3d/themes/unity/images/ui/icons/other/user-default128x128.png)

## Unity ID

A Unity ID allows you to buy and/or subscribe to Unity products and services, shop in the Asset Store and participate in the Unity community.

[Log in](login/) [Create a Unity ID](https://id.unity.com/account/new)

*   [Home](https://unity3d.com/)
*   [Products](https://unity.com/products)
*   [Solutions](https://unity.com/solutions)
*   [Made with Unity](https://unity.com/madewith)
*   [Learning](https://unity.com/learn)
*   [Support & Services](https://unity.com/support-services)
*   [Community](https://unity3d.com/unity)
    *   [Blog](https://blogs.unity3d.com/)
    *   [Forums](https://forum.unity.com/)
    *   [Answers](https://answers.unity.com/)
    *   [Evangelists](https://unity3d.com/community/evangelists)
    *   [User Groups](https://unity3d.com/community/user-groups)
    *   [Beta Program](https://unity3d.com/unity/beta)
    *   [Advisory Panel](https://unity.com/advisorypanel)

# MineCraft style game

Discussion in '[Scripting](forums/scripting.12/)' started by [chris64](members/chris64.65393/), [Apr 11, 2013](threads/minecraft-style-game.177831/).

<form action="search/search" method="post" class="formPopup"><input type="search" name="keywords" value="" class="textCtrl" placeholder="Search this thread..." title="Enter your search and hit enter" id="QuickSearchQuery"> <input type="hidden" name="type[post][thread_id]" value="177831" id="search_bar_thread" class="AutoChecker" data-uncheck="#search_bar_title_only, #search_bar_nodes"> <input type="hidden" name="_xfToken" value=""></form>

<form action="inline-mod/post/switch" method="post" class="InlineModForm section" data-cookiename="posts" data-controls="#InlineModControls" data-imodoptions="#ModerationSelect option">

1.  ### [chris64](members/chris64.65393/)

    [![chris64](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/chris64.65393/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Sep 27, 2011</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[72](search/member?user_id=65393)</dd>

    </dl>

    > OK, I know I'm the 10,000 person to ask this question...but I couldn't find anything asking the right question.  
    >   
    > I've had an idea for a game for quite awhile and my son playing minecraft has re-inspired me to pursue this project again. How on earth could I possibly manage the memory for a bunch of blocks (mincreaft style). It appears to have no small limits in any direction on the map but assumming a typical map is around 512 x 512 x 128, and 256 choices of blocks, we could easily be at over 33 meg storing that array of data, not even counting the objects and the textures. Do you think they handle this by just loading all the objects? Any thoughts or ideas on if this is possible within Unity (as it's done in Java, I'm guessing that with some clever design this is absolutely possible). If so, how about loading neighboring "biomes"? I'm hoping people want to brainstorm this concept a little bit. Just as a developer I'm facsinated with how clever this program is.

    [chris64](members/chris64.65393/), [Apr 11, 2013](threads/minecraft-style-game.177831/ "Permalink")[#1](threads/minecraft-style-game.177831/ "Permalink")
2.  ### [jessee03](members/jessee03.43700/)

    [![jessee03](data/avatars/m/43/43700.jpg?1323829666)](members/jessee03.43700/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Apr 27, 2011</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[655](search/member?user_id=43700)</dd>

    </dl>

    > chris64 said: [↑](goto/post?id=1216212#post-1216212)
    > 
    > > OK, I know I'm the 10,000 person to ask this question...but I couldn't find anything asking the right question.  
    > >   
    > > I've had an idea for a game for quite awhile and my son playing minecraft has re-inspired me to pursue this project again. How on earth could I possibly manage the memory for a bunch of blocks (mincreaft style). It appears to have no small limits in any direction on the map but assumming a typical map is around 512 x 512 x 128, and 256 choices of blocks, we could easily be at over 33 meg storing that array of data, not even counting the objects and the textures. Do you think they handle this by just loading all the objects? Any thoughts or ideas on if this is possible within Unity (as it's done in Java, I'm guessing that with some clever design this is absolutely possible). If so, how about loading neighboring "biomes"? I'm hoping people want to brainstorm this concept a little bit. Just as a developer I'm facsinated with how clever this program is.Click to expand...
    > 
    > There is a minecraft clone project floating around somewhere on the forums. It's open to anyone to download and see how it was created. Just do a quick search. I've played around with it and it seems like everything ran just great.

    [jessee03](members/jessee03.43700/), [Apr 11, 2013](threads/minecraft-style-game.177831/#post-1216250 "Permalink")[#2](threads/minecraft-style-game.177831/#post-1216250 "Permalink")
3.  ### [BFGames](members/bfgames.137207/)

    [![BFGames](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/bfgames.137207/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Oct 2, 2012</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[1,543](search/member?user_id=137207)</dd>

    </dl>

    > Just created a Voxel Engine for Unity.  
    >   
    > We simply store the voxels in Chunks of 32x32x32, in a Chunk controller holding all Chunks.  
    > Each Voxel is a simple byte, in a list, deciding the type. Each Voxel also holds a ref to a color in a color array, which is used for when drawing or changing a voxel.  
    >   
    > Sure it takes up some memory, but so does Minecraft. We tested a level the other day consisting of 150 million voxels, worked fine.  
    >   
    > However changing the world needs you to redraw chunks, and worse re calculate the mesh, which can be a bitch. I used a lot of time coming up with a smart way of changing the mesh collider without killing performance. And there is a lot of other problems with it, so be ready to spend a LOT of time creating a game like this if it should work well.  
    >   
    > We can however even create "2D" voxel worlds with great depth, from a single PNG now, and much more ![;)](styles/default/xenforo/clear.png "Wink    ;)")

    [BFGames](members/bfgames.137207/), [Apr 11, 2013](threads/minecraft-style-game.177831/#post-1216253 "Permalink")[#3](threads/minecraft-style-game.177831/#post-1216253 "Permalink")
4.  ### [chris64](members/chris64.65393/)

    [![chris64](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/chris64.65393/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Sep 27, 2011</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[72](search/member?user_id=65393)</dd>

    </dl>

    > Thanks! I will have to further research this. Perhaps I'm thinking a little too old school in CPU//memory capabilities.

    [chris64](members/chris64.65393/), [Apr 11, 2013](threads/minecraft-style-game.177831/#post-1216271 "Permalink")[#4](threads/minecraft-style-game.177831/#post-1216271 "Permalink")
5.  ### [angrypenguin](members/angrypenguin.79526/)

    [![angrypenguin](data/avatars/m/79/79526.jpg?1401852871)](members/angrypenguin.79526/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Dec 29, 2011</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[12,417](search/member?user_id=79526)</dd>

    </dl>

    > Why do you do anything with a mesh collider? I haven't done this kind of thing, but I'd have thought that primitive colliders would be lightning fast for it.

    [angrypenguin](members/angrypenguin.79526/), [Apr 11, 2013](threads/minecraft-style-game.177831/#post-1216547 "Permalink")[#5](threads/minecraft-style-game.177831/#post-1216547 "Permalink")
6.  ### [BFGames](members/bfgames.137207/)

    [![BFGames](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/bfgames.137207/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Oct 2, 2012</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[1,543](search/member?user_id=137207)</dd>

    </dl>

    > If you want it to work with unity's rigid body or char controller, then you want a mesh colliders in order to have a small amount of objects in the game

    [BFGames](members/bfgames.137207/), [Apr 11, 2013](threads/minecraft-style-game.177831/#post-1216626 "Permalink")[#6](threads/minecraft-style-game.177831/#post-1216626 "Permalink")
7.  ### [angrypenguin](members/angrypenguin.79526/)

    [![angrypenguin](data/avatars/m/79/79526.jpg?1401852871)](members/angrypenguin.79526/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Dec 29, 2011</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[12,417](search/member?user_id=79526)</dd>

    </dl>

    > BFGames said: [↑](goto/post?id=1216626#post-1216626)
    > 
    > > If you want it to work with unity's rigid body or char controller, then you want a mesh colliders in order to have a small amount of objects in the gameClick to expand...
    > 
    > What happens when the object count is too high? And what exactly are we calling high?

    [angrypenguin](members/angrypenguin.79526/), [Apr 11, 2013](threads/minecraft-style-game.177831/#post-1216729 "Permalink")[#7](threads/minecraft-style-game.177831/#post-1216729 "Permalink")
8.  ### [BFGames](members/bfgames.137207/)

    [![BFGames](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/bfgames.137207/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Oct 2, 2012</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[1,543](search/member?user_id=137207)</dd>

    </dl>

    > Performance goes to hell. We did start out by trying to use simple colliders for each voxel on an empty game object. Even small levels with a a few 1000 voxels were unplayable.  
    >   
    > And as we need more then 100 million voxels in our levels for the game we are building you can see the problem. (Newest level runs smooth with 180 million voxels, but takes some time to load ofcause)

    [BFGames](members/bfgames.137207/), [Apr 11, 2013](threads/minecraft-style-game.177831/#post-1217100 "Permalink")[#8](threads/minecraft-style-game.177831/#post-1217100 "Permalink")
9.  ### [chris64](members/chris64.65393/)

    [![chris64](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/chris64.65393/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Sep 27, 2011</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[72](search/member?user_id=65393)</dd>

    </dl>

    > So am I understanding you right here...do you recreate the meshes of the objects on the fly and/or the colliders?  
    >   
    > What I wanted to do was work with a fairly small single world (512x512x128 maybe). I was hoping I could get away with simply instantiating cubes everywhere, but in addition to that I do want to use physics. I guess I'll need to start some proof of concept apps.

    [chris64](members/chris64.65393/), [Apr 11, 2013](threads/minecraft-style-game.177831/#post-1217169 "Permalink")[#9](threads/minecraft-style-game.177831/#post-1217169 "Permalink")
10.  ### [BFGames](members/bfgames.137207/)

    [![BFGames](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/bfgames.137207/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Oct 2, 2012</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[1,543](search/member?user_id=137207)</dd>

    </dl>

    > Yes we change meshes and mesh colliders on the fly, so i can destroy and build the world. Mesh colliders is by far the biggest problem as its is extremely performance heavy, so you need to divide you chunk into as few as possible tri/verts if you do not want to wait a second everytime you change a single voxel. Also by chunking it up you got practically no draw calls, i think our level got like 25 draw calls ![:D](styles/default/xenforo/clear.png "Big Grin    :D")  
    >   
    > A 512x512x128 world is still 33 million Voxels. That is not possible with cubes.

    [BFGames](members/bfgames.137207/), [Apr 11, 2013](threads/minecraft-style-game.177831/#post-1217174 "Permalink")[#10](threads/minecraft-style-game.177831/#post-1217174 "Permalink")
11.  ### [angrypenguin](members/angrypenguin.79526/)

    [![angrypenguin](data/avatars/m/79/79526.jpg?1401852871)](members/angrypenguin.79526/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Dec 29, 2011</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[12,417](search/member?user_id=79526)</dd>

    </dl>

    > BFGames said: [↑](goto/post?id=1217100#post-1217100)
    > 
    > > Performance goes to hell. We did start out by trying to use simple colliders for each voxel on an empty game object. Even small levels with a a few 1000 voxels were unplayable.Click to expand...
    > 
    > Fair enough. I've been able to have a few thousand primitive colliders running on even mobile devices without an issue in the past, but I guess different scene structures could end up having wildly different performance characteristics.

    [angrypenguin](members/angrypenguin.79526/), [Apr 12, 2013](threads/minecraft-style-game.177831/#post-1217366 "Permalink")[#11](threads/minecraft-style-game.177831/#post-1217366 "Permalink")
12.  ### [chris64](members/chris64.65393/)

    [![chris64](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/chris64.65393/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Sep 27, 2011</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[72](search/member?user_id=65393)</dd>

    </dl>

    > BFGames said: [↑](goto/post?id=1217174#post-1217174)
    > 
    > > Yes we change meshes and mesh colliders on the fly, so i can destroy and build the world. Mesh colliders is by far the biggest problem as its is extremely performance heavy, so you need to divide you chunk into as few as possible tri/verts if you do not want to wait a second everytime you change a single voxel. Also by chunking it up you got practically no draw calls, i think our level got like 25 draw calls ![:D](styles/default/xenforo/clear.png "Big Grin    :D")  
    > >   
    > > A 512x512x128 world is still 33 million Voxels. That is not possible with cubes.Click to expand...
    > 
    > Can you give me any ideas on where I can begin researching building these dynamic meshes? I've only ever created objects with 3d software. Are you really able to reduce the triangle count? I mean, I see how you could not include meshes completely surrounded by other meshes (granted, that would be the majority especially on an undug map). I haven't explored the boundaries on minecraft but I imagine you could build an intensive mine network that would make every cube visible...does this affect it's performance?

    [chris64](members/chris64.65393/), [Apr 12, 2013](threads/minecraft-style-game.177831/#post-1217982 "Permalink")[#12](threads/minecraft-style-game.177831/#post-1217982 "Permalink")
13.  ### [hausmaus](members/hausmaus.76792/)

    [![hausmaus](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/hausmaus.76792/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Dec 9, 2011</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[105](search/member?user_id=76792)</dd>

    </dl>

    > Mikola Lysenko's blog should be a good starting point  
    > [http://0fps.wordpress.com/2012/06/30/meshing-in-a-minecraft-game/](http://0fps.wordpress.com/2012/06/30/meshing-in-a-minecraft-game/)  
    >   
    > So will a google search for "greedy meshing".  
    >   
    > I wouldn't worry too much about the polyreduction aspect, just the surface/interior testing, which by itself is pretty straightforward. Also, while memory requirements are probably not a major concern, some of the super-efficient clever code approaches are also very short and easy to implement, if you have a good intuitive grasp of what they're doing.  
    >   
    > BFGames's suggestion to work with n*n*n chunks is excellent, for many reasons but especially resurfacing and sparse updates for serialization. As for physics, if MeshCollider is too slow, you could use something like the intersection tests from XNA ([http://xbox.create.msdn.com/en-US/education/catalog/sample/collision](http://xbox.create.msdn.com/en-US/education/catalog/sample/collision)). I'm using a modified version of that for content tools for an MMO with some partitioning and additional tests (different type of content but similar performance goals).  
    >   
    > Cheers,  
    > -Adrian

    Last edited: Apr 13, 2013[hausmaus](members/hausmaus.76792/), [Apr 13, 2013](threads/minecraft-style-game.177831/#post-1218406 "Permalink")[#13](threads/minecraft-style-game.177831/#post-1218406 "Permalink")
14.  ### [chris64](members/chris64.65393/)

    [![chris64](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/chris64.65393/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Sep 27, 2011</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[72](search/member?user_id=65393)</dd>

    </dl>

    > Thanks. That was a very helpful link.  
    >   
    > The greedy meshing is interesting. I wrote something that did just for a ground map I made a while ago (iPhone didn't have the GPU for a larger terrain map). It didn't work out very well for me because of an unexpect side affect. Imagine if you have 2 cubes on row 1 and a double wide cube on row 2\. Even though they line up perfectly in design, when you start to move the camera around the center points on row 1 will not always line up correctly with row 2\. Perhaps I'm doing something wrong but in general I found that things look better if vertices meet at the same place. Am I mistaken on this? Plus, how would you handle textures?

    [chris64](members/chris64.65393/), [Apr 13, 2013](threads/minecraft-style-game.177831/#post-1218439 "Permalink")[#14](threads/minecraft-style-game.177831/#post-1218439 "Permalink")
15.  ### [hausmaus](members/hausmaus.76792/)

    [![hausmaus](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/hausmaus.76792/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Dec 9, 2011</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[105](search/member?user_id=76792)</dd>

    </dl>

    > Hm, you know, I remembered that post being more interesting, although reading over it again, it really is a lot heavier on polyreduction than I thought. I thought there was more about general implementation.  
    >   
    > Minecraft just uses culling, as far as I know, since the issues you bring up with greedy meshes are definitely problems. You can see code for Lysenko's simple culling implementation here [https://github.com/mikolalysenko/mikolalysenko.github.com/tree/gh-pages/MinecraftMeshes/js](https://github.com/mikolalysenko/mikolalysenko.github.com/tree/gh-pages/MinecraftMeshes/js). Although you can see that he does not handle winding order.  
    >   
    > But again, I think a naive implementation based on simple neighbor data would be pretty straightforward... basically if you don't have a neighbor in some direction, put two triangles there, wound appropriately (and then endless work related to caching I'd imagine). I don't think you'd want to try sharing verts, since you can't on corners or block type/texture seams anyway, but it's easy to add that if needed.  
    >   
    > As for dynamically creating meshes, it's actually really simple. Basically you are just populating buffers, the main two being vertices (Vector3[], one for each vert in the mesh), and triangles (int[], always a multiple of three of course). UV, UV2, normals, tangents, and colors are all per-vertex so you'd create them at the same time.  
    >   
    > Winding order can be tricky to get right. I have a distantly-related project on my blog ([http://adrianswall.com/?p=229](http://adrianswall.com/?p=229)), a retopologizer which goes through a Minecraft-looking phase, and although the code is overcomplicated and the problem it solves is different, the meshing code itself may be of some interest. Also, this recent post ([http://forum.unity3d.com/threads/177659-Connect-2-meshes-in-script?p=1215037&viewfull=1#post1215037](http://forum.unity3d.com/threads/177659-Connect-2-meshes-in-script?p=1215037&viewfull=1#post1215037)) creates a cylinder, and actually that's one of the smallest/clearest examples I can find of building a mesh right now! Where the triangle indices are added, you'll see it tries to reuse verts... if you instead created new verts and added their IDs for every tri, that would give you a faceted model, which is what you need for Minecraft meshing.  
    >   
    > Completely OT: Since you mentioned large terrains, (again pulling from memory here), have you looked into geoclipmapping? [http://http.developer.nvidia.com/GPUGems2/gpugems2_chapter02.html](http://http.developer.nvidia.com/GPUGems2/gpugems2_chapter02.html) That article gets lost in some weird details too, and the technique requires vertex texture fetch, something which iOS used to support but no longer does. Still an interesting concept.  
    >   
    > Cheers,  
    > -Adrian

    Last edited: Apr 13, 2013[hausmaus](members/hausmaus.76792/), [Apr 13, 2013](threads/minecraft-style-game.177831/#post-1218461 "Permalink")[#15](threads/minecraft-style-game.177831/#post-1218461 "Permalink")
16.  ### [hausmaus](members/hausmaus.76792/)

    [![hausmaus](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/hausmaus.76792/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Dec 9, 2011</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[105](search/member?user_id=76792)</dd>

    </dl>

    > Well I got into the idea and wanted to do a quick proof of concept of the quick neighbor test meshing, so here it is:  
    > ![$minecraft_meshing_1.jpg](https://forum.unity.com/attachments/minecraft_meshing_1-jpg.50061/ "Click this image to show the full-size version.")  
    >   
    > Project files: [adrianswall.com/temp/SimpleMinecraft.zip](http://adrianswall.com/temp/SimpleMinecraft.zip)  
    >   
    > This isn't optimized, but it works as described above. For demonstration purposes, it builds out a ground at y=0, and has a chance of building up on +y from there. The point is to generate some type/neighbor data to demonstrate (implicitly culled) meshing from source arrays, and handling some specifics like chunks and UVs for cell types.  
    >   
    > The code is in WizardBuildGround.cs.  
    >   
    > Lots of places to go from here, but a decent quick and dirty proof of concept.  
    >   
    > Cheers,  
    > -Adrian

    Last edited: Apr 13, 2013[hausmaus](members/hausmaus.76792/), [Apr 13, 2013](threads/minecraft-style-game.177831/#post-1218902 "Permalink")[#16](threads/minecraft-style-game.177831/#post-1218902 "Permalink")
17.  ### [BFGames](members/bfgames.137207/)

    [![BFGames](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/bfgames.137207/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Oct 2, 2012</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[1,543](search/member?user_id=137207)</dd>

    </dl>

    > hausmaus said: [↑](goto/post?id=1218902#post-1218902)
    > 
    > > Well I got into the idea and wanted to do a quick proof of concept of the quick neighbor test meshing, so here it is:  
    > > [View attachment 50061](https://forum.unity.com/attachments/50061/)  
    > >   
    > > Project files: [adrianswall.com/temp/SimpleMinecraft.zip](http://adrianswall.com/temp/SimpleMinecraft.zip)  
    > >   
    > > This isn't optimized, but it works as described above. For demonstration purposes, it builds out a ground at y=0, and has a chance of building up on +y from there. The point is to generate some type/neighbor data to demonstrate (implicitly culled) meshing from source arrays, and handling some specifics like chunks and UVs for cell types.  
    > >   
    > > The code is in WizardBuildGround.cs.  
    > >   
    > > Lots of places to go from here, but a decent quick and dirty proof of concept.  
    > >   
    > > Cheers,  
    > > -AdrianClick to expand...
    > 
    > Any colliders on there?

    [BFGames](members/bfgames.137207/), [Apr 14, 2013](threads/minecraft-style-game.177831/#post-1219376 "Permalink")[#17](threads/minecraft-style-game.177831/#post-1219376 "Permalink")
18.  ### [hausmaus](members/hausmaus.76792/)

    [![hausmaus](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/hausmaus.76792/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Dec 9, 2011</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[105](search/member?user_id=76792)</dd>

    </dl>

    > No Unity colliders. As I was saying in a previous post, I've been using a modified version of an XNA intersection test suite (just a bunch of functions). This allows custom partitioning, return types, content-specific optimization, etc. It can also be run on a server alongside or instead of Jitter, which is nice as well.  
    >   
    > This was done quickly mostly to answer the OP's followup question about meshing. But I was thinking about collision quite a bit.  
    >   
    > Raycasting (for mining) doesn't need intersection, just use the cell type map, player position, and orientation. It's not even an AABB test because it's snapped to a grid too.  
    >   
    > I'm not sure if this is true, but there might be some benefits to making local updates to chunk meshes, which requires knowing which polys belong to which cubes (cubeXYZ->first triangle index... since all verts and tris are built per cell in order in this example, you only need offset and number of faces to get polys). That's a lot of storage of course so I don't know how practical it is, although I do know it can be built at at almost no cost.  
    >   
    > For character collision, type data and pooling AABB instances would work, or storing polys per cell. Or some basic partitioning, or maybe even none at all since the chunks already provide some. And I think you can do it without the capsule too, since there are no hip-level colliders in Minecraft. So everything could be two spheres and some AABBs. At least for this primitive terrain, and then per-poly tests for props or ramps.  
    >   
    > So I like the collider-free approach, although it adds a lot of work on top of this simple meshing example.  
    >   
    > Regards,  
    > -Adrian

    [hausmaus](members/hausmaus.76792/), [Apr 14, 2013](threads/minecraft-style-game.177831/#post-1219572 "Permalink")[#18](threads/minecraft-style-game.177831/#post-1219572 "Permalink")
19.  ### [DryTear](members/drytear.160355/)

    [![DryTear](data/avatars/m/160/160355.jpg?1420260797)](members/drytear.160355/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Nov 30, 2012</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[312](search/member?user_id=160355)</dd>

    </dl>

    > Im making an Minecraft like game too, but anyways i surfed thru the web and i found these two usefull links:  
    >   
    > [http://www.forum.unity3d.com/thread...se-in-progress-(terrain-generation-etc-).html](http://www.forum.unity3d.com/threads/83316-3D-Perlin-Noise-in-progress-(terrain-generation-etc-).html)  
    >   
    > [http://www.answers.unity3d.com/questions/177931/tutorial-minecraft-chunks.html](http://www.answers.unity3d.com/questions/177931/tutorial-minecraft-chunks.html)  
    >   
    > Hope it helps :]

    [DryTear](members/drytear.160355/), [Apr 14, 2013](threads/minecraft-style-game.177831/#post-1219591 "Permalink")[#19](threads/minecraft-style-game.177831/#post-1219591 "Permalink")
20.  ### [chris64](members/chris64.65393/)

    [![chris64](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/chris64.65393/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Sep 27, 2011</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[72](search/member?user_id=65393)</dd>

    </dl>

    > Thanks for all the replies. Hausmaus, I couldn't get your code to do anything. Was it complete? DryTear, none of your links were working for me. I finally got a chance this morning to start digging into this and my licesnses are messed with the latest Unity, none of the samples are working, none of the links are working...off to a great start ![:p](styles/default/xenforo/clear.png "Stick Out Tongue    :p"). I always forget how much trouble it is to reset up my UI after an update.

    [chris64](members/chris64.65393/), [Apr 14, 2013](threads/minecraft-style-game.177831/#post-1219621 "Permalink")[#20](threads/minecraft-style-game.177831/#post-1219621 "Permalink")
21.  ### [hausmaus](members/hausmaus.76792/)

    [![hausmaus](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/hausmaus.76792/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Dec 9, 2011</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[105](search/member?user_id=76792)</dd>

    </dl>

    > Chris,  
    >   
    > Yes, my example is complete, if you downloaded the project. You should find a "Minecraft" option in the main menu, with only one entry. Click that, and it will do its thing. Note that it can be run many times, it will just destroy the previous output.  
    >   
    > I have a modified version here which also does AO (seemed easy enough to add and I'm an AO whore) which I can post if that one still doesn't work.

    [hausmaus](members/hausmaus.76792/), [Apr 14, 2013](threads/minecraft-style-game.177831/#post-1219625 "Permalink")[#21](threads/minecraft-style-game.177831/#post-1219625 "Permalink")
22.  ### [DryTear](members/drytear.160355/)

    [![DryTear](data/avatars/m/160/160355.jpg?1420260797)](members/drytear.160355/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Nov 30, 2012</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[312](search/member?user_id=160355)</dd>

    </dl>

    > Try now, if it still doesnt work- try to copy and paste the link into the address bar

    [DryTear](members/drytear.160355/), [Apr 14, 2013](threads/minecraft-style-game.177831/#post-1219842 "Permalink")[#22](threads/minecraft-style-game.177831/#post-1219842 "Permalink")
23.  ### [PlaybucksGames](members/playbucksgames.134120/)

    [![PlaybucksGames](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/playbucksgames.134120/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Sep 21, 2012</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[4](search/member?user_id=134120)</dd>

    </dl>

    > ![$sasasaa1.png](https://forum.unity.com/attachments/sasasaa1-png.63968/ "Click this image to show the full-size version.")  
    >   
    > what's this?  
    >   
    > how can i fix this problem?

    [PlaybucksGames](members/playbucksgames.134120/), [Aug 16, 2013](threads/minecraft-style-game.177831/#post-1330789 "Permalink")[#23](threads/minecraft-style-game.177831/#post-1330789 "Permalink")
24.  ### [Uncasid](members/uncasid.137889/)

    [![Uncasid](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/uncasid.137889/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Oct 5, 2012</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[193](search/member?user_id=137889)</dd>

    </dl>

    > BFGames said: [↑](goto/post?id=1219376#post-1219376)
    > 
    > > Any colliders on there?Click to expand...
    > 
    > I am surprised you are using mesh colliders! If you are using square based voxels, it is much easier to do intersect testing!

    [Uncasid](members/uncasid.137889/), [Aug 16, 2013](threads/minecraft-style-game.177831/#post-1331068 "Permalink")[#24](threads/minecraft-style-game.177831/#post-1331068 "Permalink")
25.  ### [hausmaus](members/hausmaus.76792/)

    [![hausmaus](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/hausmaus.76792/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Dec 9, 2011</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[105](search/member?user_id=76792)</dd>

    </dl>

    > Playbucks,  
    >   
    > If it were consistent, it would look like the boundaries within your atlas are wrong (something is 65px wide instead of 64px) or you have a rounding error for UVs on one side of the cube. But it isn't consistent so a simple explanation like that doesn't seem like it would help.  
    >   
    > Can you please explain how you are generating your mesh?

    [hausmaus](members/hausmaus.76792/), [Aug 16, 2013](threads/minecraft-style-game.177831/#post-1331089 "Permalink")[#25](threads/minecraft-style-game.177831/#post-1331089 "Permalink")
26.  ### [PlaybucksGames](members/playbucksgames.134120/)

    [![PlaybucksGames](styles/UnitySkin/xenforo/avatars/avatar_m.png)](members/playbucksgames.134120/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>Sep 21, 2012</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[4](search/member?user_id=134120)</dd>

    </dl>

    > hausmaus said: [↑](goto/post?id=1331089#post-1331089)
    > 
    > > Playbucks,  
    > >   
    > > If it were consistent, it would look like the boundaries within your atlas are wrong (something is 65px wide instead of 64px) or you have a rounding error for UVs on one side of the cube. But it isn't consistent so a simple explanation like that doesn't seem like it would help.  
    > >   
    > > Can you please explain how you are generating your mesh?Click to expand...
    > 
    > After receiving your files(`adrianswall.com/temp/SimpleMinecraft.zip`), it does not have a modify.  
    > Just open and run..

    [PlaybucksGames](members/playbucksgames.134120/), [Aug 21, 2013](threads/minecraft-style-game.177831/#post-1335035 "Permalink")[#26](threads/minecraft-style-game.177831/#post-1335035 "Permalink")
27.  ### [christides11](members/christides11.107406/)

    [![christides11](https://secure.gravatar.com/avatar/ba8e8f6bbb938d4488f84d67933c9aea?s=96&d=https%3A%2F%2Fforum.unity.com%2Fstyles%2FUnitySkin%2Fxenforo%2Favatars%2Favatar_male_m.png)](members/christides11.107406/) 

    <dl class="pairsJustified">

    <dt>Joined:</dt>

    <dd>May 19, 2012</dd>

    </dl>

    <dl class="pairsJustified">

    <dt>Posts:</dt>

    <dd>[667](search/member?user_id=107406)</dd>

    </dl>

    > We have a HUUUUGE thread already on this.  
    > [http://forum.unity3d.com/threads/63149-After-playing-minecraft](http://forum.unity3d.com/threads/63149-After-playing-minecraft)

    [christides11](members/christides11.107406/), [Aug 21, 2013](threads/minecraft-style-game.177831/#post-1335068 "Permalink")[#27](threads/minecraft-style-game.177831/#post-1335068 "Permalink")

<input type="hidden" name="_xfToken" value=""></form>

<label for="LoginControl">[(You must log in or sign up to reply here.)](login/)</label>[Show Ignored Content](javascript: "Show hidden content by ")

<form action="login/login" method="post" class="xenForm eAuth" id="login" style="display:none">

<dl class="ctrlUnit">

<dt><label for="LoginControl">Your name or email address:</label></dt>

<dd><input type="text" name="login" id="LoginControl" class="textCtrl" tabindex="101"></dd>

</dl>

<dl class="ctrlUnit">

<dt><label for="ctrl_password">Password:</label></dt>

<dd><input type="password" name="password" class="textCtrl" id="ctrl_password" tabindex="102">[Forgot your password?](lost-password/)</dd>

</dl>

<dl class="ctrlUnit submitUnit">

<dd><input type="submit" class="button primary" value="Log in" tabindex="104" data-loginphrase="Log in" data-signupphrase="Sign up"> <label for="ctrl_remember" class="rememberPassword"><input type="checkbox" name="remember" value="1" id="ctrl_remember" tabindex="103"> Stay logged in</label></dd>

</dl>

<input type="hidden" name="cookie_check" value="1"> <input type="hidden" name="redirect" value="/threads/minecraft-style-game.177831/"> <input type="hidden" name="_xfToken" value=""></form>

[](https://forum.unity.com/goofytown.php)[![](/styles/UnitySkin/custom/clear.gif)](https://forum.unity.com/goofytown.php)[unity](https://forum.unity.com/goofytown.php)[unity](https://forum.unity.com/goofytown.php)[](https://forum.unity.com/goofytown.php)[unity](https://forum.unity.com/goofytown.php)[unity](https://forum.unity.com/goofytown.php)[](https://forum.unity.com/goofytown.php)[](/)<a class="to-top"></a>

Purchase

*   [Subscription](https://store.unity.com/)
*   [Asset Store](https://assetstore.unity.com/)
*   [Unity Gear](https://unity3d.com/gear)
*   [Resellers](https://store.unity.com/resellers)

Education

*   [Students](https://store.unity.com/academic/unity-student)
*   [Educators](https://unity.com/education#unity-for-educators-and-academic-institutions)
*   [Center of Excellence](https://unity.com/coe)

Download

*   [Unity](https://store.unity.com/)
*   [Beta Program](https://unity3d.com/unity/beta)
*   [Web Player](https://unity3d.com/webplayer)
*   [Press material](https://unity3d.com/public-relations/downloads)
*   [Whitepapers](https://unity3d.com/whitepapers)

Unity Labs

*   [Labs](https://unity.com/labs)
*   [Publications](https://unity.com/labs/publications)

Resources

*   [Learn](https://unity3d.com/learn)
*   [Community](https://unity3d.com/community)
*   [Documentation](https://unity3d.com/learn/documentation)
*   [Unity QA](https://unity3d.com/unity/qa)
*   [FAQ](https://unity3d.com/unity/faq)
*   [Services Status](http://status.cloud.unity3d.com/)
*   [Certification](https://certification.unity.com)
*   [Connect](https://connect.unity.com/)

About Unity

*   [Blog](http://blogs.unity3d.com/)
*   [Events](https://unity3d.com/events)
*   [Careers](https://careers.unity.com)
*   [Contact](https://unity3d.com/contact)
*   [Press](https://unity3d.com/public-relations)
*   [Partners](https://unity.com/partners)
*   [Affiliates](https://unity3d.com/affiliates)
*   [Security](https://unity3d.com/security)

<form method="post" action="/contact/newsletter-signup" id="newsletter-signup" class="newsletter-signup rel">

Get Unity news

<input type="email" name="email" id="email" value="" class="email mb15" placeholder="Enter your email here..."> <input type="text" name="name" id="name" value="" class="name mb15 hide" placeholder="Enter your name here..."> <input type="submit" value="Sign up" class="btn bg-dg"><input type="checkbox" id="agree" name="terms" value="agreed"> <label for="agree">I agree to the [Unity Privacy Policy](https://unity3d.com/company/legal/privacy-policy) and the processing and use of my information</label>

#### Nearly there...

To start receiving news from Unity Technologies, click on the link we've sent to your e-mail account.

#### Oops...

Something went wrong, please try again.

</form>

Language

*   [中文](/cn)
*   [Français](/fr)
*   [Deutsch](/de)
*   [日本語](/jp)
*   [한국어](/kr)
*   [Português](/pt)
*   [Русский](/ru)
*   [Español](/es)

[Facebook](http://www.facebook.com/unity3d) [Twitter](http://www.twitter.com/unity3d) [Instagram](https://www.instagram.com/unitytechnologies) [LinkedIn](https://www.linkedin.com/company/unity-technologies) [YouTube](https://www.youtube.com/user/Unity3D)© 2020 Unity Technologies

*   [Legal](https://unity3d.com/legal)
*   [Privacy Policy](https://unity3d.com/legal/privacy-policy)
*   [Cookies](https://unity3d.com/legal/cookie-policy#cookies)

"Unity", Unity logos, and other Unity trademarks are trademarks or registered trademarks of Unity Technologies or its affiliates in the U.S. and elsewhere (more info [here](https://unity3d.com/legal/trademarks)). Other names or brands are trademarks of their respective owners.<style>.qc-cmp-showing { visibility: hidden !important; } body.didomi-popup-open { overflow: auto !important; } #didomi-host { visibility: hidden !important; }</style>   ×

<form class="sj-search-form"><input data-sj-search-query="" type="search" placeholder="search this site"> Search</form>