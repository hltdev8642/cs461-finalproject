<h2 id="falling-sand">Falling Sand</h2>
<p>In this lab, you’ll create a <em>falling sand</em> program. The software resembles a paint program, except that the user is painting particles into the world. The software simulates the physical behavior of those particles, which may move (perhaps falling like grains of sand), change, clone, disappear, interact, etc.</p>
<hr />
<h3 id="exercise-0-getting-started">Exercise 0: Getting Started</h3>
<p>Download <a href="FallingSandWithoutArrays.zip">FallingSandWithoutArrays.zip</a>. Compile and run <u>SandLab.java</u>. (This will run <code>SandLab</code>’s <code>main</code> method, which constructs a new <code>SandLab</code> and calls its <code>run</code> method.) You should see a window pop up. On the left side is a black rectangular canvas which will soon be inhabited by particles. On the right side there is one button for each tool you will be able to paint with: <em>Empty</em> (for erasing) and <em>Metal</em> (for creating metal particles). You can’t actually paint now, because you haven’t written the code yet.</p>
<p>Look in the <u>SandLab.java</u> file, and you’ll see that a <code>SandLab</code> remembers a single thing:</p>
<ul>
<li><code>display</code> - the <code>SandDisplay</code> used to show the particles on the screen</li>
</ul>
<p>A summary of the methods provided by the SandDisplay class is shown below.</p>
<blockquote>
<pre>**<u>class SandDisplay</u>**
int getNumRows()
int getNumCols()
int getValue(int row, int col)  //returns particle type value currently stored at given location
void setValue(int row, int col, int value)  //stores particle type value at given location (does not change what appears on screen)
void setColor(int row, int col, Color color)  //changes what color appears at given location
</pre>
</blockquote>
<hr />
<p>Notice that we’re using <code>int</code> values to represent particle types, with 0 representing <em>empty</em>, 1 representing <em>metal</em>, and higher values representing the additional particle types you’ll be adding. To avoid confusion, <strong>we never want to see these particle type numbers (0, 1, etc.) in our code!</strong> Instead, we’ve declared variables for each of these types. You’ll see these listed near the top of <u>SandLab.java</u>.</p>
<blockquote>
<pre>public static final int EMPTY = 0;
public static final int METAL = 1;
</pre>
</blockquote>
<p>This lets us use meaningful variable names instead of confusing type numbers in our code. For example:</p>
<blockquote>
<pre>if (type == METAL)
</pre>
</blockquote>
<p>These variables are marked <code>final</code> to indicate that they are constants. (Attempts to re-assign to these variables will not compile.) By convention in Java, we use all-caps names for constants. (Traditionally, constants are also declared as <code>public</code> and <code>static</code>, so that we can access them from outside the file by writing <code>SandLab.METAL</code>, for example.)</p>
<hr />
<h3 id="exercise-1-constructor">Exercise 1: Constructor</h3>
<p>Familiarize yourself with the <code>SandLab</code> constructor, which already initializes the <code>display</code> field to refer to a new <code>SandLabDisplay</code> with appropriate dimensions and tool names.</p>
<p>There is no code for you to write in this exercise.</p>
<hr />
<h3 id="exercise-2-locationclicked">Exercise 2: <code>locationClicked</code></h3>
<p>The <code>locationClicked</code> method is called (by the <code>run</code> method) whenever the user clicks on some part of the canvas. The selected tool (<em>empty</em>, <em>metal</em>, etc.) is passed to the method. Simply call <code>SandDisplay</code>’s <code>setValue</code> method to store this value at the given coordinates.</p>
<p>(You won’t be able to test this code yet.)</p>
<hr />
<h3 id="exercise-3-updatedisplay">Exercise 3: <code>updateDisplay</code></h3>
<p>The <code>updateDisplay</code> method is called (by the <code>run</code> method) at regular intervals. Its job is to draw each particle (and empty space) onto the display, using <code>SandDisplay</code>’s <code>getValue</code> method to get the particle type value at given coordinates, and using <code>SandDisplay</code>’s <code>setColor</code> method to show an appropriate color for that particle type at those coordinates. Complete this method so that empty locations are shown in one color (probably black) and metal locations are shown in another color (probably gray).</p>
<blockquote>
<pre>**<u>class java.awt.Color</u>**
Color(int red, int green, int blue) // values range from 0 - 255 inclusive
</pre>
</blockquote>
<p>Test that you can now paint metal particles and erase them.</p>
<hr />
<h3 id="exercise-4-sand">Exercise 4: Sand</h3>
<p>Modify your program so that you can also paint with <em>sand</em> particles (probably in yellow). For now, these particles won’t actually move.</p>
<hr />
<h3 id="exercise-5-step">Exercise 5: <code>step</code></h3>
<p>The <code>step</code> method is called (by the <code>run</code> method) at regular intervals. This method should choose a <em>single random valid location</em>. (Do not use a loop.) If that location contains a sand particle and the location below it is empty, the particle should move down one row. (Metal particles will never move.) This code should call <code>SandDisplay</code>’s <code>setValue</code> method. (Do not call <code>setColor</code>.)</p>
<p>Test that your sand particles fall now.</p>
<p><strong>Tip:</strong> If particles fall too quickly or too slowly, the speed can be adjusted by adjusting the slider in the display or by changing the dimensions passed to the <code>SandLab</code> constructor (from <code>main</code>).</p>
<p><strong>Note:</strong> Because the <code>step</code> method picks a single random particle to move (or act in some way) each time it is called, it is possible that some sand particles will move several times before others have the chance to move at all. In practice, the <code>step</code> method is called so rapidly that you are unlikely to notice this effect when you run the code.</p>
<hr />
<h3 id="exercise-6-water">Exercise 6: Water</h3>
<p>Modify your program so that you can also paint with <em>water</em> particles, which move in one of three randomly chosen directions: down, left, or right.</p>
<p>In the <code>step</code> method, when the randomly chosen location contains a water particle, pick one of three random directions. If the location in that randomly chosen direction is empty, the water particle moves there. (Look for ways to minimize duplicate code in your <code>step</code> method.)</p>
<p>Test that the water behaves roughly like a liquid, taking the shape of a container.</p>
<hr />
<h3 id="exercise-7-dropping-sand-into-water">Exercise 7: Dropping Sand Into Water</h3>
<p>What happens now when you drop sand particles into water? Right now, sand is only allowed to move into empty spaces. Modify your code so that a sand particle can also move into a space containing a water particle (by trading places with the water particle). (Look for ways to minimize duplicate code in your <code>step</code> method.) Test that you can drop sand into water now (without destroying the water).</p>
<hr />
<h3 id="now-implement-other-behaviors-for-additional-credit.-get-creative">Now implement other behaviors for additional credit. Get creative!’)</h3>
