//See SimWorldGrid
class ActivitySquare {
   int x1, y1, x2, y2;
   protected int activeCells;
   
   ActivitySquare( int init_x1, int init_y1, int init_x2, int init_y2 ){
     this.x1 = init_x1;
     this.y1 = init_y1;
     this.x2 = init_x2;
     this.y2 = init_y2;
     
     this.activeCells = 0;
   }
   
   ActivitySquare(){
     activeCells = 0;
     x1 = y1 = x2 = y2 = 0;
   }
   
   public int getActiveCells(){
     return activeCells;
   }
   
   public void setActiveCells( int newValue ){
     activeCells = newValue;
   }
   
   final void incActiveCells(){
     this.activeCells++;
   }
   
   final void decActiveCells(){
     this.activeCells--;
   }
}

class NullActivitySquare extends ActivitySquare {
   NullActivitySquare(){
     super();
   }
  
   NullActivitySquare( int init_x1, int init_y1, int init_x2, int init_y2 ){
     super( init_x1, init_y1, init_x2, init_y2 );
   }
   
   public int getActiveCells(){
     return 0;
   }
}
