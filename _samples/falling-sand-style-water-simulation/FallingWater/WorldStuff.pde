//Utility functions

PVector screen2world( PVector v ){
  v.div( zoom_level );
  v.sub( translationVect );
  v.div( TILE_SIZE );
  v.y = theWorld.world_height - v.y;
  return v;
}

PVector screen2world( float x, float y ){
  PVector temp = new PVector( x, y );
  return screen2world(temp);
}

void resetCameraPos(){
  //Reset camera position
  zoom_level = 1.0;
  camera_pos.x = -width/2 + TILE_SIZE;
  camera_pos.y = -height/2 + TILE_SIZE*2;
}
