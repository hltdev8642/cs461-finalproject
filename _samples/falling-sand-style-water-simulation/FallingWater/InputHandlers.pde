//Keyboard event handler
void keyPressed(){
  if (key == CODED) return;

  switch(key){
    case 's':  //Move camera down
    case 'S': 
      camera_dir.y = -1;
      break;
  
    case 'w':  //Move camera up
    case 'W': 
      camera_dir.y = 1;
      break;
  
    case 'a':  //Move camera left
    case 'A': 
      camera_dir.x = 1;
      break;
  
    case 'd':  //Move camera right
    case 'D': 
      camera_dir.x = -1;
      break;
  
    case '+': //Zoom in
    case '=':
      zoom_dir = 1;
      break;
  
    case '-': //Zoom out
      zoom_dir = -1;
      break;
  
    case '0': //Reset camera position
      resetCameraPos();
      break;
      
    case '1': //Paint ground tiles
      selectedTileType = ground;
      break;
      
    case '2': //Paint water tiles
      selectedTileType = water;
      break;
      
    case '3': //Paint faucets
      selectedTileType = faucet;
      break;
      
    case 'p':  //Pause the simulation / switch to step-by-step mode
    case 'P': 
      paused = !paused;
      break;
    
    case 'c':  //Clear the map
    case 'C':
      theWorld.clear();
      break;
      
    case 'r':  //Reinitialize the map with random cells
    case 'R':
      theWorld.randomize();
      break;
      
    case 'u':  //Simulate the worst-case scenario - a map completely filled
    case 'U':  //with falling water, no inactive tiles
      theWorld.worstCaseWorld();
      break;
      
    default: //For any other key, step the simulation if it's currently paused
      if ( paused ){
        theWorld.stepSimulation();
      }
  }

}

//Key handler for when a key is released
void keyReleased(){
  if (key == CODED) return;

  switch(key){
  case 's':  //Stop moving down
  case 'S': 
    if (camera_dir.y == -1)
      camera_dir.y = 0;
    break;

  case 'w':  //Stop moving up
  case 'W': 
    if (camera_dir.y == 1)
      camera_dir.y = 0;
    break;

  case 'a':  //Stop moving left
  case 'A': 
    if (camera_dir.x == 1)
      camera_dir.x = 0;
    break;

  case 'd':  //...right.
  case 'D': 
    if (camera_dir.x == -1)
      camera_dir.x = 0;
    break;

  case '+': //Stop zooming in
  case '=':
    if ( zoom_dir == 1) 
      zoom_dir = 0;
    break;

  case '-': //Stop zooming out
    if ( zoom_dir == -1) 
      zoom_dir = 0;
    break;
  }
}

//Mousewheel event
void mouseWheel(int delta) {
  //Zoom in/out when scrolling the mouse wheel
  zoom_level = zoom_level - delta*zoom_speed*5;
}


/**
Mousewheel handler
@author Rick Companje
*/
 
void initWheelListener() {
  addMouseWheelListener(new java.awt.event.MouseWheelListener() { 
    public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) { 
      mouseWheel(evt.getWheelRotation());
  }}); 
}
