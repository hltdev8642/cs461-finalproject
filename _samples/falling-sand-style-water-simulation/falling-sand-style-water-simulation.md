# [W-Shadow.com](https://w-shadow.com)

A blog about web development, software business, and WordPress

*   [Home](/)
*   [WordPress Plugins](/wordpress-plugins/)
*   [Popular Posts](/popular-posts/)
*   [Contact](/contact/)

## [How To Make a “Falling Sand” Style Water Simulation](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/ "Permanent Link: How To Make a “Falling Sand” Style Water Simulation")

Have you ever wondered how all those [“falling sand” games](http://chir.ag/stuff/sand/) work under the hood? If so, read on. Today I will discuss one of the possible ways how you could implement the “falling” part of the game – sand particles falling under the effects of gravity, water (or other liquids) flowing down a hillside, and so on. [Source code](http://w-shadow.com/files/FallingWater.zip) and a [live demo](http://w-shadow.com/files/FallingSandWater/) are also available. This tutorial is along the same vein as my previous [fluid simulation post](http://w-shadow.com/blog/2009/09/01/simple-fluid-simulation/) that dealt with pseudo-compressible fluids and pressure, but the specific algorithm is much simpler (and probably quite a bit faster, too).

_Side-note : No, I haven’t been completely consumed by an inexorable desire to build fun graphical toys instead of practically useful software, why do you ask? 🙂 In fact, there’ll be some WordPress-related announcements later this week…_

### Learning To <del datetime="2009-09-28T14:15:43+00:00">Fly</del> Fall

![Falling water movement](https://i1.wp.com/w-shadow.com/wp-content/uploads/2009/09/gravity-simulation-rules.png?resize=120%2C236&ssl=1 "Falling water movement")While I can’t claim intimate familiarity with the algorithms the aforementioned games use to simulate fire or explosions, it’s pretty easy to come up with a way to implement falling sand/flowing water. First, you need to figure out under what conditions something can fall/flow downwards. For a basic simulation this could be just a small set of very simple rules :

*   For each entity that is affected by gravity
    *   If there’s an empty space **below** it, move it **down**.
    *   If there’s an empty space **down and to the left**, move it **down and to the left**.
    *   If there’s an empty space **down and to the right**, move it **down and to the right**.

Next, you need to decide how to represent the simulation world and all the objects (like immovable walls, moving water particles _et cetera_) that it contains. One of the most common solutions is to use a two-dimensional array where each array element is an numeric ID of the material contained in that map tile. For example, 0 = air (or an empty square), 1 = wall, 2 = water, and so on.

![Map array](https://i0.wp.com/w-shadow.com/wp-content/uploads/2009/09/map-representation.png?resize=481%2C306&ssl=1 "Map array")

In most falling sand games each array element would correspond to a single pixel on the game’s screen. However, in my example application I used 20×20 pixel tiles instead to make the underlying simulation behaviour easier to see.

If possible, use _byte_ or _char_ as the array element datatype. Using _int’s_ might seem more straightforward, but it would also take up at least four times more memory and hurt performance. It’s also a good idea to make the array just a bit bigger than the actual map dimensions and create a single-tile “buffer zone” around the map so that you can avoid checking for boundary conditions during the simulation.

Finally, during the simulation step we just loop over the entire array and apply the simple rules I mentioned above to each tile that contains material that should be affected by gravity.

### Getting It Right

![Now that's just silly.](https://i1.wp.com/w-shadow.com/wp-content/uploads/2009/09/whee.png?resize=78%2C211&ssl=1 "Now that's just silly.")If you go ahead and implement the above algorithm as written, you will get a simulation that _kind-of_ works, but quickly grinds to a halt on big maps and sometimes behaves in unexpected ways. For example, you might see sand tiles that seem to “teleport” to the bottom of the map instead of falling gradually and water that always flows _left_ after hitting a free-standing ground tile (even though there’s a perfectly suitable channel available on the right side). Lets see what we can do about that.

**Keeping track of updated tiles**

The “teleportation” behaviour can happen if our simulation algorithm doesn’t keep track of which tiles it has already moved around during the current simulation step. For example, first it loops over the top row of the map, finds a water tile A and moves it downwards one row. Then it loops over the second row, finds the tile A, moves it again, goes to the third row, moves the same water tile _again_… and so on, until the water tile A hits a solid tile or falls off the map.

To prevent this bug we need to somehow remember which tiles have been updated already. Since I wanted to keep the memory use low, I used the most-significant bit of each map array element to indicate whether that tile was last updated on an even-numbered simulation step (0) or an odd-numbered step (1). Then, at the start of each step, the algorithm calculates what the flag should be for tiles updated in that step :

<pre>update_mask = byte ( ( simulationFrameCount & (long)1 ) << 7 );
//this basically takes the modulo of frame-count / 2 and shifts the result 7 places to the left</pre>

…then sets the msb of each tile it updates to that value and automatically skips tiles that have it already set to that value.

**Pseudo-random movement**

When a falling particle hits a free-standing fixed tile on a 2D map it has two choices – it can either move down and left, or down and right. In the naive algorithm I discussed above it will always try to move left first, leading to weirdness. Obviously we want it to choose at random, but on a big map generating a new random number for each tile would be slow. Instead, lets use the same approach as for the “teleportation” problem and move it either diagonally left if the current simulation step is even-numbered and or diagonally right if it’s odd-numbered. This can still introduce some artifacts in the simulation behaviour, but they’re rarer and and significantly less noticeable.

### Optimization

Speaking of optimization, we don’t really need to update each gravity-enabled tile every time. Many of them will eventually end up in a situation where they can’t move anywhere (e.g. water at the bottom of a puddle or sand resting on a flat surface), so it would be nice if we could quickly skip them and not waste our time trying to apply the simulation rules to these static tiles. This would help performance quite a bit.

The way I implemented is with the help with another bit-flag. If the simulation algorithm discovers that it can’t move a tile in any direction, it will set the tile’s “static” bit-flag. It will also skip any tiles that have the “static” bit-flag set.

There’s some additional book-keeping we need to do to make this work as intended. When an empty space is created somewhere in the map (e.g. by a water tile moving down) we need to re-activate any gravity-enabled tiles that are directly above the newly created hole, so that they can fall/flow down into that space.

### Possible Improvements

The result is a pretty basic and plain simulation that is useful to explain the basic principle, but it wouldn’t really qualify as a full game. Here are some ideas you could use to improve it :

*   Make water flow horizontally if there’s another water tile on top of it. Consider adding a simple pressure model [link to my prev. article].
*   Implement fluid density. This one is easy – if the current tile is more dense than the tile below it, swap them.
*   Add tile generators, e.g. a faucet that creates a water tile below itself on each simulation step (included in my demo application).
*   To improve performance, use a [quadtree](http://en.wikipedia.org/wiki/Quadtree) to keep track of which regions of the map have interacting tiles and quickly skip inactive regions.
*   Explore the source code of existing games and look for interesting ideas. Some open-source examples : [wxSand](http://www.piettes.com/fallingsandgame/), [SDL Sand](http://sourceforge.net/projects/sdlsand/).

### Live Demo & Source

Like my previous [fluid simulation app](http://w-shadow.com/blog/2009/09/01/simple-fluid-simulation/), this one was also written in [Processing](http://processing.org/). In addition to a working implementation of the algorithm discussed above it also lets you draw and erase water/ground/faucet tiles using your mouse, pan around the map with the WASD keys, zoom in/out with the mouse wheel and more. However, be warned that the map rendering code is not very optimized so the simulation _will_ degenerate into a slideshow if you zoom all the way out, regardless of how fast your system is.

*   [Live demo  (requires Java)](http://w-shadow.com/files/FallingSandWater/)
*   [Source code](http://w-shadow.com/files/FallingWater.zip)

**Related posts :**

*   [Simple Fluid Simulation With Cellular Automata](https://w-shadow.com/blog/2009/09/01/simple-fluid-simulation/)
*   [Minecraft – Building Castles In The Sand](https://w-shadow.com/blog/2009/06/15/minecraft-review/)
*   [Move Steam To a Different Disk](https://w-shadow.com/blog/2007/10/11/move-steam-to-a-different-disk/)
*   [Celtic Knot Generator – A HTML5 Canvas Experiment](https://w-shadow.com/blog/2012/05/17/celtic-knot-generator-canvas/)
*   [Improved Thread Simulation Class for PHP](https://w-shadow.com/blog/2008/05/24/improved-thread-simulation-class-for-php/)
*   [Camera Shake Is Not Realistic In FPS](https://w-shadow.com/blog/2008/08/17/camera-shake-is-not-realistic-in-fps/)
*   [Fast Weighted Random Choice In PHP](https://w-shadow.com/blog/2008/12/10/fast-weighted-random-choice-in-php/)

This entry was posted on Tuesday, September 29th, 2009 at 17:14 and is filed under [Fun Projects](https://w-shadow.com/category/projects/fun-projects/). You can follow any responses to this entry through the [RSS 2.0](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/feed/) feed. You can [leave a response](#respond), or [trackback](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/trackback/) from your own site.« [Simple Fluid Simulation With Cellular Automata](https://w-shadow.com/blog/2009/09/01/simple-fluid-simulation/) | [How To Make WordPress Check For Updates Immediately](https://w-shadow.com/blog/2009/10/01/force-update-check-plugin/) »

### 14 Responses to “How To Make a “Falling Sand” Style Water Simulation”

1.  ![](https://secure.gravatar.com/avatar/875e39a3995c9be912fd65cb96c113a1?s=32&d=mm&r=r) <cite class="fn">[Ross Shannon](http://rossshannon.com)</cite> says:[September 30, 2009 at 16:00](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/#comment-31618)

    Good tutorial, thanks. I don’t understand the problem with particles teleporting downwards though — it seems to be that the only reasonable way to program this is to do the simulation updates bottom-to-top. Otherwise, if you update top-to-bottom, when you update the game state array, any particles that have another particle above them will be overwritten by the top particle falling into their position?

2.  ![](https://secure.gravatar.com/avatar/9de2919e66bdb845db55df54d7028402?s=32&d=mm&r=r) <cite class="fn">[White Shadow](http://w-shadow.com/)</cite> says:[September 30, 2009 at 16:14](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/#comment-31619)

    You are correct, updating bottom-to-top is a good approach. I didn’t mention it in the post, but that’s exactly what my demo app does.

    However, that alone wouldn’t be enough to prevent “teleportation” in a more complex simulation. For example, if you added a rule like “if this fluid particle has another fluid particle above it, move this one sideways” (i.e. a very basic pressure factor), you’d get particles teleporting left or right depending on the direction of your X loop. So tracking which particles have been updated still makes sense.

3.  ![](https://secure.gravatar.com/avatar/8d421f07cabb003f0460485b125c3c9c?s=32&d=mm&r=r) <cite class="fn">Henk</cite> says:[August 28, 2010 at 00:22](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/#comment-81956)

    Neat tutorial, I followed it on a bored friday evening :). I implemented the teleporting thing using a simple ‘updated’ boolean 2d array that sets fields to true if they’ve been updated during that round, but that’s mainly because I’m not much of a bit twiddler.

    Haven’t got the water going horizontal right yet though. It’d probably become either very complex, or need to be rewritten if I ever add other materials.

4.  ![](https://secure.gravatar.com/avatar/03c96ddb2d78bcae761081efd9dad6cd?s=32&d=mm&r=r) <cite class="fn">Grant</cite> says:[December 22, 2010 at 00:18](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/#comment-148857)

    There is a very easy fix for the “teleporting” problem. Before the main loop (updating every tile), copy the entire world into a “copy” array. Then, whenever a particle is updated, it gets its information about the world from the original array, but modified the copy array. At the end of the main loop, you copy every value from the “copy” array into the origininal array. The reason it works is: every particle gets its information from a non-updated array! No particle can influence another particle DURING the updating process!

5.  ![](https://secure.gravatar.com/avatar/03c96ddb2d78bcae761081efd9dad6cd?s=32&d=mm&r=r) <cite class="fn">Grant</cite> says:[December 22, 2010 at 00:19](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/#comment-148859)

    Whoops! Instead of “…but modified the copy array…”, it should read “…but modifies the copy array…”

6.  ![](https://secure.gravatar.com/avatar/9de2919e66bdb845db55df54d7028402?s=32&d=mm&r=r) <cite class="fn">[White Shadow](http://w-shadow.com/)</cite> says:[December 22, 2010 at 02:31](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/#comment-148927)

    True, but I suspect performance would suffer if you copied the entire world (twice!) every frame.

7.  ![](https://secure.gravatar.com/avatar/d0f80f17f5f0ae2d4e450338b0159b1b?s=32&d=mm&r=r) <cite class="fn">shpen</cite> says:[July 13, 2011 at 04:38](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/#comment-171380)

    I’m having problems with creating an even falling pattern. Because I am iterating through each sand particle one by one, from left to right, the sand tends to fall to the left, since a slot on the left is opened before one on the right. Is there any way to fix this and cause an even falling pattern?

8.  ![](https://secure.gravatar.com/avatar/9de2919e66bdb845db55df54d7028402?s=32&d=mm&r=r) <cite class="fn">[White Shadow](http://w-shadow.com/)</cite> says:[July 13, 2011 at 09:49](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/#comment-171396)

    Hmm, I don’t see how that could happen. If you’re iterating left to right, bottom up, the entire lower line should fall down “at once”, from the perspective of the simulation.

9.  ![](https://secure.gravatar.com/avatar/d0f80f17f5f0ae2d4e450338b0159b1b?s=32&d=mm&r=r) <cite class="fn">shpen</cite> says:[July 13, 2011 at 21:47](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/#comment-171420)

    I’m actually going from top to bottom, but bottom to top still causes the same problem.

    Maybe you can take a look at my source and help me figure out what’s wrong:
    [http://dl.dropbox.com/u/77961/GameTemplate.java](http://dl.dropbox.com/u/77961/GameTemplate.java)

    Also, here’s a jar which will show you what the problem looks like:
    [http://dl.dropbox.com/u/77961/SandTest.jar](http://dl.dropbox.com/u/77961/SandTest.jar)

    Thanks!

10.  ![](https://secure.gravatar.com/avatar/9de2919e66bdb845db55df54d7028402?s=32&d=mm&r=r) <cite class="fn">[White Shadow](http://w-shadow.com/)</cite> says:[July 14, 2011 at 11:59](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/#comment-171433)

    Ah, now I see it.

    One possible way to fix the problem would be to only allow a sand particle to move left/right if both the corresponding diagonal _and_ horizontal neighbours are empty:

    <pre lang="java">if ((x > 0) && (sand[x - 1][y + 1] == 0) && (sand[x - 1][y] == 0)) {
         left = true;
    }

    if ((x < (WIDTH - 1)) && (sand[x + 1][y + 1] == 0) && (sand[x + 1][y] == 0)) {
         right = true;
    }
    </pre>

    (I haven't tried this myself.)

11.  ![](https://secure.gravatar.com/avatar/115aeb951385840da592bb03c646b027?s=32&d=mm&r=r) <cite class="fn">TenCashMan</cite> says:[February 21, 2012 at 06:10](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/#comment-198257)

    I would like to say thanks a lot for this post!
    I have been looking for something like this!

    With this post I was able to put together a small falling sands app for the psp!
    Thanks man!

    Also, In some falling sands games (well, most actually) you see particles have random left and right movements as they fall, would it be wise to implement that into the program?

12.  ![](https://secure.gravatar.com/avatar/9de2919e66bdb845db55df54d7028402?s=32&d=mm&r=r) <cite class="fn">[Jānis Elsts](http://w-shadow.com/)</cite> says:[February 21, 2012 at 12:00](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/#comment-198263)

    Just try it and see how it plays. That’s probably the best advice I can give.

13.  ![](https://secure.gravatar.com/avatar/115aeb951385840da592bb03c646b027?s=32&d=mm&r=r) <cite class="fn">TenCashMan</cite> says:[March 13, 2012 at 04:23](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/#comment-198682)

    Ok, I tried to make another falling sands kinda game for PC (c++ and allegro) and I am running into alot of trouble.

    Sand doesn’t fall down correctly going to the… right side. It stacks up and flows down one layer at a time, but it works fine going to the right side.
    Also sometimes when it goes to the right side, it forms “cacti” so to speak. The water will sometimes start going in random directions before falling the down.

    It’s probably pretty hard to look at, but here is the if statements for when water should move.
    [http://pastebin.com/ZSP71r8S](http://pastebin.com/ZSP71r8S)

    I would appreciate any help.

14.  ![](https://secure.gravatar.com/avatar/e3ecfe3197138a97201199a9dcb2e491?s=32&d=mm&r=r) <cite class="fn">[feee3.com](http://www.feee3.com/search_result.aspx?s=5)</cite> says:[July 14, 2013 at 05:50](https://w-shadow.com/blog/2009/09/29/falling-sand-style-water-simulation/#comment-317266)

    Thanks for any other magnificent post. Where else could anyone get that type of information in such a perfect method of writing? I have a presentation subsequent week, and I’m at the search for such information.

### Leave a Reply

[Click here to cancel reply.](/blog/2009/09/29/falling-sand-style-water-simulation/#respond)

<form action="https://w-shadow.com/wp-comments-post.php" method="post" id="commentform">

<input type="text" name="author" id="author" value="" size="22" tabindex="1" aria-required="true"> <label for="author">Name (required)</label>

<input type="text" name="email" id="email" value="" size="22" tabindex="2" aria-required="true"> <label for="email">Mail (will not be published) (required)</label>

<input type="text" name="url" id="url" value="" size="22" tabindex="3"> <label for="url">Website</label>

<textarea autocomplete="new-password" id="comment" name="f86fc09519" cols="100%" rows="10" tabindex="4"></textarea><textarea id="a56f95b067863c4cbfd64eeb12273e16" aria-hidden="true" name="comment" autocomplete="new-password" style="padding:0;clip:rect(1px, 1px, 1px, 1px);position:absolute !important;white-space:nowrap;height:1px;width:1px;overflow:hidden;" tabindex="-1"></textarea>

<input name="submit" type="submit" id="submit" tabindex="5" value="Submit Comment"> <input type="hidden" name="comment_post_ID" value="1295" id="comment_post_ID"> <input type="hidden" name="comment_parent" id="comment_parent" value="0">

<input type="hidden" id="akismet_comment_nonce" name="akismet_comment_nonce" value="e9a3bd47ee">

<input type="text" name="nxts" value="1588532546"><input type="text" name="nxts_signed" value="8b38a21a439b731ca4f796d0750066d2591fab8c"><input type="text" name="0960b2951763b60cc7764ee6d495" value="d7600f6cc9bd07dff"><input type="text" name="a4bb7d0a9cb2645d81d" value="">

<input type="hidden" id="ak_js" name="ak_js" value="1588544436059"></form>

*   ## RSS Feed

    [![](https://i1.wp.com/www.feedburner.com/fb/images/pub/feed-icon32x32.png) Subscribe via RSS](http://feeds.feedburner.com/wshadowcom "Subscribe to my feed")

    [![](https://i2.wp.com/w-shadow.com/wp-content/themes/contempt/images/mail-icon-32x32.png?resize=32%2C32) Get Updates by Email](http://feedburner.google.com/fb/a/mailverify?uri=wshadowcom&loc=en_US)

*   ## Recent Posts

    *   [Plugin Updates: Securing Download Links](https://w-shadow.com/blog/2013/03/19/plugin-updates-securing-download-links/)
    *   [WordPress Update Server](https://w-shadow.com/blog/2013/03/12/wordpress-update-server/)
    *   [Plugin Compatibility Reporter](https://w-shadow.com/blog/2012/12/04/plugin-compatibility-reporter/)
    *   [How to Pre-Select a Category for a New Post](https://w-shadow.com/blog/2012/11/20/pre-select-category-for-new-post/)
    *   [Magic Quotes in WordPress, and How To Get Rid of Them](https://w-shadow.com/blog/2012/11/13/wordpress-magic-quotes/)
*   [![](https://i2.wp.com/w-shadow.com/wp-content/uploads/2012/09/01_200x200_01-sharper.png?resize=200%2C200)](http://w-shadow.com/admin-menu-editor-pro/?utm_source=w-shadow.com&utm_medium=banner&utm_content=ame-banner-rightbanners-1&utm_campaign=Blogs "Admin Menu Editor Pro")
*   ## Categories

    *   [Announcements](https://w-shadow.com/category/announcements/)
    *   [Blogging](https://w-shadow.com/category/blogging/)
    *   [Business](https://w-shadow.com/category/business/)
    *   [Miscellany](https://w-shadow.com/category/miscellany/)
    *   [Projects](https://w-shadow.com/category/projects/)
        *   [Browser Extensions](https://w-shadow.com/category/projects/extensions/)
        *   [Desktop Apps](https://w-shadow.com/category/projects/desktop-apps/)
        *   [Fun Projects](https://w-shadow.com/category/projects/fun-projects/)
        *   [Web Apps](https://w-shadow.com/category/projects/web-apps/)
        *   [WordPress Plugins](https://w-shadow.com/category/projects/plugins/)
        *   [WordPress Tools](https://w-shadow.com/category/projects/wordpress-tools/)
    *   [Web Development](https://w-shadow.com/category/web-development/)
        *   [WordPress Development](https://w-shadow.com/category/web-development/wordpress-development/)
*   ## Search

    <form action="http://www.google.com/cse" id="cse-search-box"><input type="hidden" name="cx" value="partner-pub-5378330175394346:6127561002"> <input type="hidden" name="ie" value="UTF-8"> <input type="text" name="q" style="width: 97%"> <input type="submit" name="sa" value="Search"></form>

[Privacy policy](/privacy-policy/)
Cthulhu generated this page with 62 queries, in 1.101 seconds.

<link rel="stylesheet" id="yarppRelatedCss-css" href="https://w-shadow.com/wp-content/plugins/yet-another-related-posts-plugin/style/related.css?ver=5.4.1" type="text/css" media="all">    *   <form method="post"><input type="submit" value="Close and accept" class="accept"></form>

    Privacy & Cookies: This site uses cookies. By continuing to use this website, you agree to their use.
    To find out more, including how to control cookies, see here: [Privacy Policy](https://adminmenueditor.com/privacy-policy/)
    }
    }