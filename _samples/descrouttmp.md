# js-powdertoy

Javascript Powder Toy.
Just another TPT clone.
The Powder Toy - February 2020
==========================

Get the latest version [from the Powder Toy website](https://powdertoy.co.uk/Download.html).

To use online features such as saving, you need to [register an account](https://powdertoy.co.uk/Register.html).
You can also visit [the official TPT forum](https://powdertoy.co.uk/Discussions/Categories/Index.html).

Have you ever wanted to blow something up? Or maybe you always dreamt of operating an atomic power plant? Do you have a will to develop your own CPU? The Powder Toy lets you to do all of these, and even more!

The Powder Toy is a free physics sandbox game, which simulates air pressure and velocity, heat, gravity and a countless number of interactions between different substances! The game provides you with various building materials, liquids, gases and electronic components which can be used to construct complex machines, guns, bombs, realistic terrains and almost anything else. You can then mine them and watch cool explosions, add intricate wirings, play with little stickmen or operate your machine. You can browse and play thousands of different saves made by the community or upload your own – we welcome your creations!

There is a Lua API – you can automate your work or even make plugins for the game. The Powder Toy is free and the source code is distributed under the GNU General Public License, so you can modify the game yourself or help with development. TPT is compiled using scons.

Build instructions
===========================================================================

See the _Powder Toy Development Help_ section [on the main page of the wiki](https://powdertoy.co.uk/Wiki/W/Main_Page.html).

Thanks
===========================================================================

* Stanislaw K Skowronek - Designed the original
* Simon Robertshaw
* Skresanov Savely
* cracker64
* Catelite
* Bryan Hoyle
* Nathan Cousins
* jacksonmj
* Felix Wallin
* Lieuwe Mosch
* Anthony Boot
* Matthew "me4502"
* MaksProg
* jacob1
* mniip
* LBPHacker


Instructions
===========================================================================

Click on the elements with the mouse and draw in the field, like in MS Paint. The rest of the game is learning what happens next.


Controls
===========================================================================

| Key                     | Action                                                          |
| ----------------------- | --------------------------------------------------------------- |
| TAB                     | Switch between circle/square/triangle brush                     |
| Space                   | Pause                                                           |
| Q / Esc                 | Quit                                                            |
| Z                       | Zoom                                                            |
| S                       | Save stamp (use with Ctrl when STK2 is out)                     |
| L                       | Load last saved stamp                                           |
| K                       | Stamp library                                                   |
| 0-9                     | Set view mode                                                   |
| P / F2                  | Save screenshot as .png                                         |
| E                       | Bring up element search                                         |
| F                       | Pause and step to next frame                                    |
| G                       | Increase grid size                                              |
| Shift + G               | Decrease grid size                                              |
| H                       | Show/Hide HUD                                                   |
| Ctrl + H / F1           | Show intro text                                                 |
| D / F3                  | Debug mode (use with Ctrl when STK2 is out)                     |
| I                       | Invert Pressure and Velocity map                                |
| W                       | Cycle gravity modes (use with Ctrl when STK2 is out)            |
| Y                       | Cycle air modes                                                 |
| B                       | Enter decoration editor menu                                    |
| Ctrl + B                | Toggle decorations on/off                                       |
| N                       | Toggle Newtonian Gravity on/off                                 |
| U                       | Toggle ambient heat on/off                                      |
| Ctrl + I                | Install powder toy, for loading saves/stamps by double clicking |
| Backtick                | Toggle console                                                  |
| =                       | Reset pressure and velocity map                                 |
| Ctrl + =                | Reset Electricity                                               |
| \[                      | Decrease brush size                                             |
| \]                      | Increase brush size                                             |
| Alt + \[                | Decrease brush size by 1                                        |
| Alt + \]                | Increase brush size by 1                                        |
| Ctrl + C/V/X            | Copy/Paste/Cut                                                  |
| Ctrl + Z                | Undo                                                            |
| Ctrl + Y                | Redo                                                            |
| Ctrl + Cursor drag      | Rectangle                                                       |
| Shift + Cursor drag     | Line                                                            |
| Middle click            | Sample element                                                  |
| Alt + Left click        | Sample element                                                  |
| Mouse scroll            | Change brush size                                               |
| Ctrl + Mouse scroll     | Change vertical brush size                                      |
| Shift + Mouse scroll    | Change horizontal brush size                                    |
| Shift + R               | Horizontal mirror for selected area when pasting stamps         |
| Ctrl + Shift + R        | Vertical mirror for selected area when pasting stamps           |
| R                       | Rotate selected area counterclockwise when pasting stamps       |



Command Line
---------------------------------------------------------------------------

| Command               | Description                                      | Example                           |
| --------------------- | ------------------------------------------------ | --------------------------------- |
| `scale:SIZE`          | Change window scale factor                       | `scale:2`                         |
| `kiosk`               | Fullscreen mode                                  |                                   |
| `proxy:SERVER[:PORT]` | Proxy server to use                              | `proxy:wwwcache.lancs.ac.uk:8080` |
| `open FILE`           | Opens the file as a stamp or game save           |                                   |
| `ddir DIRECTORY`      | Directory used for saving stamps and preferences |                                   |
| `ptsave:SAVEID`       | Open online save, used by ptsave: URLs           | `ptsave:2198`                     |
| `disable-network`     | Disables internet connections                    |                                   |
| `redirect`            | Redirects output to stdout.txt / stderr.txt      |                                   |
# UniPowder

This is a Unity version of a *[Falling Sand Game](https://en.wikipedia.org/wiki/Falling-sand_game)* or a *[Powder Toy Game](https://en.wikipedia.org/wiki/The_Powder_Toy)*.

Basically the game allows you to add one pixel particle (AKA powders) of different elements (sand, water, acid...) on screen and each of these element is simulated individually as it interacts with other elements.

This uses the newly release *[Entities Component Systems](https://github.com/Unity-Technologies/EntityComponentSystemSamples)* unity library to parallized entities simulation.

These are resources that have influenced and help the creation of this project:

- [ECS Annoncement](https://unity3d.com/unity/features/job-system-ECS)
- [The Official Powder Toy app](http://powdertoy.co.uk/)
- [Powder Toy sources](https://github.com/ThePowderToy/The-Powder-Toy)
- [Powder Toy in Javascript](https://github.com/SopaXorzTaker/js-powdertoy)
- [Powder Clone in C#](https://github.com/ruarai/PowderClone/tree/master/PowderClone)
- [Powder game in java](https://github.com/daveyognaught/powderinjava)

## Element interactions

- Water remove fire
- Fire burns wood and turns it into smoke or fire.
- Acid destroy everything except Glass
- Lava destroy evertyhing except Stone
- Steam condenses on Stone and turns into water
- Water turns into steam when touching lava
- Sand turns into glass when eated by fire.

![interactions](imgs/interactions.gif)

## Generators
Most elements who are not solid (everythign except Stone, Wood and Glass) can have a *generator* that will poured new elements each frame.

![generators](imgs/generators.gif)

