function render(canvas, particles){
	var ctx = canvas.getContext("2d");
	ctx.fillStyle = "#000000";
	ctx.fillRect(0, 0, canvas.width, canvas.height);
	for (var i = 0; i < particles.length;  i++) {
		part = particles[i];
		if (part){
			ctx.fillStyle = PARTICLE_COLOR[part.type];
			ctx.fillRect(part.x*4, part.y*4, 4, 4);
		}
	}
}
