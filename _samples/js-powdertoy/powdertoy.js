const PARTICLE_SAND	 = 0;
const PARTICLE_WOOD	 = 1;
const PARTICLE_SNDG	 = 2;
const PARTICLE_FIRE	 = 3;
const PARTICLE_FIRG	 = 4;
const PARTICLE_WATER = 5;
const PARTICLE_WTRG	 = 6;
const PARTICLE_STONE = 7;
const PARTICLE_SMOKE = 8;
const PARTICLE_STEAM = 9;
const PARTICLE_ACID = 10;
const PARTICLE_GLASS = 11;
const PARTICLE_COUNT = 12;

const PARTICLE_NAME = ["Sand", "Wood", "Sand Generator", "Fire", "Fire Generator", "Water", "Water Generator", "Stone", "Smoke", "Steam", "Acid", "Glass"];
const PARTICLE_COLOR = ["#eeee10", "#bf9c1d", "#ff00ff", "#ff0000", "#ffff00", "#0000ff", "#ffccff", "#7f7f7f", "#878787", "#e3e3e3", "#ff33ee", "#404040"];

function random(min, max){
	return min + Math.floor(Math.random() * ((max+1)-min))
}

Particle = function(x, y, type, life){
	this.x = x;
	this.y = y;
	this.type = type;
	this.life = life;
}

Simulation = function(xbound, ybound){
	this.xbound = xbound;
	this.ybound = ybound;
	this.particles = [];
	this.addParticle = function(particle){
		return this.particles.push(particle);
	};
	this.removeParticle = function(partNumber){
		this.particles[partNumber] = null;
	};
	this.getParticle = function(partNumber){
		return this.particles[partNumber];
	};
	this.findParticles = function(x, y){
		found = [];
		for (var i = 0; i < this.particles.length; i++){
			part = this.particles[i];
			if (part){
				if ((part.x == x) && (part.y == y)){
					found.push(i);
				}
			}
		}
		return found;
	};
	this.tick = function(){
		var particlesAbove;
		for (var i = 0; i < this.particles.length; i++){
					var part = this.particles[i];
			if (!part){
				continue;
			}
			else if (!(0 <= part.x && part.x < this.xbound && 0 <= part.y && part.y < this.ybound)){
				this.removeParticle(i);
				continue;
			} else if (part.life){
				part.life--;
			}
			switch(part.type){
				case PARTICLE_SAND:
					if (this.findParticles(part.x, part.y+1).length == 0){
						this.particles[i].y++;
					 
					} else if (this.findParticles(part.x-1, part.y).length == 0 && random(0, 5)){
						this.particles[i].x--;
					} else if (this.findParticles(part.x+1, part.y).length == 0 && random(0, 5)){
											this.particles[i].x++;
									}
					break;
				case PARTICLE_SNDG:
					if (this.findParticles(part.x, part.y+1).length == 0){
						newPart = new Particle(part.x, part.y+1, PARTICLE_SAND);
						this.addParticle(newPart);
					}
					break;
				case PARTICLE_FIRE:
					if (part.life == null){
						this.particles[i].life = 50;
						break;
					} else if (part.life == 0){
						this.removeParticle(i);
						break;
					}
					particlesAbove = this.findParticles(part.x, part.y-1)
					if (particlesAbove.length == 0){
						this.particles[i].y--;
					} else if (this.findParticles(part.x-1, part.y).length == 0 && random(0, 1)){
						this.particles[i].x--;
					} else if (this.findParticles(part.x+1, part.y).length == 0 && random(0, 1)){
						this.particles[i].x++;
					} else {
						for (var j = 0; j < particlesAbove.length; j++) {
							part2 = particlesAbove[j];
							if (this.getParticle(part2).type == PARTICLE_WOOD){
								this.particles[part2].type = random(0, 1)?PARTICLE_FIRE:PARTICLE_SMOKE;
							} else if (this.getParticle(part2).type == PARTICLE_SAND){
								this.particles[part2].type = PARTICLE_GLASS;
							}
						}
						this.removeParticle(i);
					}
					break;

				case PARTICLE_FIRG:
					if (this.findParticles(part.x, part.y-1).length == 0){
						newPart = new Particle(part.x, part.y-1, PARTICLE_FIRE, 50);
						this.addParticle(newPart);
					}
					break;

				case PARTICLE_WATER:
					if (this.findParticles(part.x, part.y+1).length == 0){
						this.particles[i].y++;

					} else if (this.findParticles(part.x-1, part.y).length == 0 && random(0, 1)){
						this.particles[i].x--;
					} else if (this.findParticles(part.x+1, part.y).length == 0 && random(0, 1)){
						this.particles[i].x++;
					} else {
						var particlesBelow = this.findParticles(part.x, part.y+1);
						for(var j = 0; j < particlesBelow.length; j++){
							part2 = particlesBelow[j];
								if (this.particles[part2].type == PARTICLE_FIRE){
									this.particles[part2].type = PARTICLE_STEAM;
									this.removeParticle(part);
								}
						}
					}
					break;

				case PARTICLE_WTRG:
					if (this.findParticles(part.x, part.y+1).length == 0){
						newPart = new Particle(part.x, part.y+1, PARTICLE_WATER);
						this.addParticle(newPart);
					}
					break;
					
				case PARTICLE_STEAM:
				case PARTICLE_SMOKE:
					if (part.life == null){
						this.particles[i].life = 50;
						break;
					} else if (part.life == 0){
						this.removeParticle(i);
						break;
					}
					particlesAbove = this.findParticles(part.x, part.y-1)
					if (particlesAbove.length == 0 && random(0, 5)){
						this.particles[i].y--;
					} else if (this.findParticles(part.x-1, part.y).length == 0 && random(0, 1)){
							this.particles[i].x--;
					} else if (this.findParticles(part.x+1, part.y).length == 0 && random(0, 1)){
							this.particles[i].x++;
					} else {
						for (var j = 0; j < particlesAbove.length; j++) {
							part2 = particlesAbove[j];
							if (this.getParticle(part2).type == PARTICLE_STONE && part.type == PARTICLE_STEAM){
								this.particles[i].type = PARTICLE_WATER;
							}
						}
					}
					break;
					
				case PARTICLE_ACID:
					if ((this.findParticles(part.x, part.y+1).length == 0) && random(0, 1)){
						this.particles[i].y++;
					}
					if ((this.findParticles(part.x-1, part.y).length == 0) && random(0, 1)){
						this.particles[i].x--;
					} else if ((this.findParticles(part.x+1, part.y).length == 0) && random(0, 1)){
						this.particles[i].x++;
					} else {
						var particlesNearby = this.findParticles(part.x, part.y+1).concat(this.findParticles(part.x, part.y-1)).concat(this.findParticles(part.x+1, part.y)).concat(this.findParticles(part.x-1, part.y));
						for(var j = 0; j < particlesNearby.length; j++){
							var part2 = particlesNearby[j];
							var p = this.getParticle(part2).type;
							if (!(p == PARTICLE_ACID || p == PARTICLE_FIRE || p == PARTICLE_WATER || p == PARTICLE_GLASS)){
								this.removeParticle(part2);
							}
						}
					}
					break;

			}
		}

	};
	
	this.getParticles = function(){
		return this.particles;
	}
	
	this.setParticles = function(particles){
		this.particles = particles;
	}
	
	this.reset = function(){
		this.particles = [];
	}
}
